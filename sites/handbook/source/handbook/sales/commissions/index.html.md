---
layout: handbook-page-toc
title: Commissions
---



## **Charter**

Sales Commissions is a part of Field Operations, reporting into Sales Operations. Our goal is to incentivize the field to close business, collaborate to make GitLab successful and process commissions in an accurate and timely fashion. We aim to be viewed as a trusted business partner for Field Sales.

## **Meet The Team**

- Lisa Puzar, Senior Manager, Sales Commissions
- Swetha Kashyap, Sales Commissions Manager
- Rachel Davies, Sales Commissions Analyst

## **What's New?**

Exciting news for FY22! We are migrating our existing commission system to Xactly Incent! More information to follow, however, some key features are shown below:

#### Key Benefits of Xactly Incent

- Refreshes multiple times per day for real-time estimated commission payout visibility, resulting in fewer adjustments after payroll.
- Visibility into payout estimates on open opportunities with Incentive Estimator feature.
- Incentive statements on a single layout instead of having to open individual monthly statements.
- Manager access to their team's individual statements.

![Xactly1](/handbook/sales/commissions/picturea.png)

![Xactly2](/handbook/sales/commissions/pictureb.png)

## **How to Communicate with Us**

There are two ways to connect with us.

- Via email sales-comp@gitlab.com
- Via the Chatter feature in Salesforce

## **Quick Reference Guide**

| **GETTING STARTED** | **QUOTAS** | **COMMISSION PLANS** |
| --------------- | ------ | ---------------- |
| [**FY21 Sales Compensation Plan**](/handbook/finance/sales-comp-plan/)<br><br>[**FY21 Commission Program**](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g153a2ed090_0_63) | [**Quotas Overview**](/handbook/sales/commissions/#quotas-overview)<br><br>[**Quota Components**](/handbook/sales/commissions/#quota-components)<br><br>[**Month 1 Ramp Schedule: 15 Day Rule**](/handbook/sales/commissions/#month-1-ramp-schedule)<br><br>[**Seasonality Assumptions**](/handbook/sales/commissions/#seasonality-assumptions) | [**Commissions Overview**](/handbook/sales/commissions/#commissions-overview)<br><br>[**FY21 Commission Program Terminology**](/handbook/sales/commissions/#fy21-commission-program-terminology)<br><br>[**Commissions Rules**](/handbook/sales/commissions/#commissions-rules)<br><br>[**Commission Hold and Releases**](/handbook/sales/commissions/#hold-and-releases)<br><br>[**Participant Schedules**](/handbook/sales/commissions/#participant-schedules)<br><br>[**Opportunity Splits**](/handbook/sales/commissions/#opportunity-splits) |

## **Quotas Overview**

All employees with variable compensation plans will be assigned a quota that supports their assigned territory/region shortly after the beginning of each fiscal year or when they enter into their role. Quota assignment and quota ramp (including any mid-year adjustments) are determined by Sales Leadership. Sales Leadership reserves the right to make any adjustments to quotas.

The quota coverage types are:

1. **Native Quota Rep (NQR)** - individual owns direct quota for their assigned territory.
1. **Overlay Quota Rep (OQR)** - individual supports an assigned territory that is currently owned by a NQR.

### **Quota Components**

Can be made of various measurements, but typically by ARR at the Territory, Region or Segment levels.

The current Commission Plan [presentation](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g153a2ed090_0_63) outlines the key components in detail.

### **Month 1 Ramp Schedule**

Effective February 1st, 2021, we will be utilizing a 15 day rule to determine "Month 1" of your Ramp Schedule.  If you are hired during the 1st-15th of any given month, that is considered your "Month 1", and if you are hired after the 15th of any given month, then there is a 0% contribution of that month's quota, and your "Month 1" begins on the first of the following month.
Example:

- Hired on 4/10/2021:   Month 1 = April 2021
- Hired on 4/22/2021:   Month 1 = May 2021

#### **Enterprise Ramp Schedule**

For all enterprise native-quota carrying salespeople, we assume around a ten (10) month ramp before a rep is fully productive. We have the following ramp up schedule and measure quota performance based on this schedule:

- Month 1: 0% contribution of monthly target
- Month 2: 0% contribution of monthly target
- Month 3: 10% contribution of monthly target
- Month 4: 10% contribution of monthly target
- Month 5: 30% contribution of monthly target
- Month 6: 30% contribution of monthly target
- Month 7: 60% contribution of monthly target
- Month 8: 60% contribution of monthly target
- Month 9: 75% contribution of monthly target
- Month 10: 75% contribution of monthly target
- Month 11+: 100% contribution of monthly target

#### **Mid Market Ramp Schedule**

For all Mid Market native-quota carrying salespeople, we assume around a five (5) month ramp before a rep is fully productive. We have the following ramp up schedule and measure quota performance based on this schedule:

- Month 1: 25% contribution of monthly target
- Month 2: 25% contribution of monthly target
- Month 3: 50% contribution of monthly target
- Month 4: 50% contribution of monthly target
- Month 5: 75% contribution of monthly target
- Month 6+: 100% contribution of monthly target

#### Channel Account Manager Ramp Schedule

For all Channel Account Managers, we assume around a three (3) month ramp before a rep is fully productive. This ramp is layered into the Partner Soured quota only.  Partner Assist and Partner Fulfilled quotas are not ramped.  We have the following ramp up schedule and measure quota performance based on this schedule:

- Month 1: 25% contribution of monthly target
- Month 2: 50% contribution of monthly target
- Month 3: 75% contribution of monthly target
- Month 4+: 100% contribution of monthly target

#### Internal Transfer Ramp Schedule

For all internal transfer native-quota carrying salespeople, we assume around a three (3) month ramp before a rep is fully productive. We have the following ramp up schedule and measure quota performance based on this schedule:

- Month 1: 25% contribution of monthly target
- Month 2: 50% contribution of monthly target
- Month 3: 75% contribution of monthly target
- Month 4+: 100% contribution of monthly target

#### **Seasonality Assumptions**

We also factor in seasonality into our calculations. We expect most of our business to close in the second half of the year. Our Worldwide seasonality assumptions are as follows:

- First Fiscal Quarter: 19.0%
- Second Fiscal Quarter: 24.5%
- Third Fiscal Quarter: 28.0%
- Fourth Fiscal Quarter: 28.5%

Each native-quota carrying salesperson carries an individual seasonality based on the specific circumstances of their accounts, territory, region and segment.  The above seasonality is the aggregation of GitLab as a whole, and is not specific to one person or team.  Xactly will provide Quotas by Month and Quarter.

#### **Overlay Quotas Proration**

1. Overlay Quota Reps:  Effective February 1st, 2021, we utilize a 15 day rule to determine the first month in which your quota takes place, where if you are hired during the 1st-15th of any given month, that is considered your "Month 1", and if you are hired after the 15th of any given month, then there is a 0% contribution of that month's quota, and your "first month of quota begins on the first of the following month.

Example:

- Hired on 4/10/2021:   Month 1 = April 2021
- Hired on 4/22/2021:   Month 1 = May 2021

## **Commissions Overview**

Most commission plans are paid monthly; however there are some plans that are paid on a quarterly basis. Most commissions plans are calculated in CaptivateIQ.

### **Base / Variable Split by Role**

| Job Family | Split Base | Split Variable |
| ---------- | ---------- | -------------- |
| SMB Account Executive | 50 | 50 |
| Account Executive - Mid Market | 50 | 50 |
| Strategic Account Leader | 50 | 50 |
| Area Sales Manager | 50 | 50 |
| Manager, Public Sector Inside Sales | 50 | 50 |
| Inside Sales Rep | 50 | 50 |
| Channel Sales Manager | 60 | 40 |
| Channel Services Manager | 70 | 30 |
| Solutions Architect | 75 | 25 |
| Technical Account Manager | 75 | 25 |
| Professional Services Engineer | 90 | 10 |
| Demo Systems Engineer | 90 | 10 |
| Professional Services Practice Manager | 85 | 15 |
| Professional Services Technical Instructor | 90 | 10 |
| Professional Services Engagement Manager | 80 | 20 |

### **Commission Rates**

You can find the definitions of **Base Commission Rate (BCR)** and **Super Commission Rates (SCR)** in the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/#definitions) page under the Definitions section.

## **FY21 Commission Program Terminology**

### **Base Commission Rate (BCR)**

Base Commission Rate (BCR) is the commission rate applicable to all salespeople that are on a commission plan. BCR is calculated as Annual OTI (On Target Incentive) / Full and/or Normal Quota and is the base rate to which multipliers are applied to arrive at the accelerated rates.

### **Super Commission Rate (SCR)**

New salespeople who join after the fiscal year may be eligible for a Super Commission Rate (SCR) which is calculated as Prorated OTI / Prorated Quota. OTI is prorated based on their start date while their Quota is prorated based on ramp and seasonality assumptions.
[Example of SCR](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g6ee04e54a5_0_47)

David Hong, VP of Field Operations, explains how the Super Commission Rate works:
<!-- blank line -->
<figure class="video_container"><iframe src="https://www.youtube.com/embed/2RVm2OmDuOo" height="315" width="560"></iframe></figure>

### **Channel Neutral Compensation**

FY21 commissions will be channel neutral for all deals thru partners (including Distributors, if applicable), which means standard partner discounts are credited back to the salesperson and must follow [order processing procedures](/handbook/business-ops/order-processing/) to correctly flag partner attribution. Total IACV on the deal after all discounts will count towards quota credit, but the channel neutral amount does not qualify for quota credit and only pays out for compensation at BCR. See Channel Neutral section referenced in the [FY21 Commission Plan presentation](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g7da31a6494_6_0)

### **Multi Year Commissions**

Salespeople are eligible to earn multi year commissions for IACV and Channel Neutral if the opportunity term is more than 12 months and the customer prepays the entire Total Contract Value (TCV). Outyear IACV and Channel Neutral will be paid at 50% of BCR and will not be counted towards quota credit.

## **Commissions Rules**

The following rules will apply for the various roles within the organization:

| **Role** | **Rules** | **Explanation** |
| ---- | ----- | ----------- |
| Strategic Account Leader,<br>Mid Market Account Executive | Is the Opportunity Owner | Opportunity owner will receive the commission |
| SMB Customer Advocate | Account Owner is SMB AND Account Owner is on SMB AE’s Regional Team | Account owner is an SMB team member AND the Account Owner is on regional team, SMB AE will receive credit |
| Area Sales Manager (ASM) | Opportunity Owner is subordinate AND<br>Account Owner is on Regional Team | If the Opportunity owner is a subordinate AND the Account Owner is on the ASM's regional team, the ASM will receive credit |
| Regional Director (RD) | Account Owner is your subordinate | The opportunity owner may be on a different regional team, but if the Account Owner is on the RD's team, the RD will receive credit. It is possible to override if the RD of the opportunity owner requests credit for the deal |
| Vice President (Sales) | Account Owner is your subordinate | The opportunity owner may be on a different Regional team, but if the Account Owner is on the VP's team, the VP will receive credit |
| Inside Sales Representatives - Public Sector (ISR) | Sub-Industry | The opportunity owner may be anyone on the PubSec team so long as the sub-industry is the one supported by the ISR |
| Solutions Architects (SA) | Opportunity Owner is on your regional team | The opportunity owner must be on the SA's team for them to receive credit |
| Technical Account Manager (TAM) | Opportunity Owner is on your regional team | The opportunity owner must be on the TAM's team for them to receive credit |

## **Hold and Releases**

Commissions are paid when the opportunity is invoiced in full. If the opportunity is not invoiced in full no commissions will be paid. These commissions will be held in the Commission system and will be released by the billing team when invoiced in full. In case of a special situation where the commissions need to be released even though not invoiced in full we would need written approval from the CRO and CFO to release those commissions.

## **Participant Schedules**

Within the first few weeks of your employment, you should receive your participant schedule. This document will provide details on your plan components, territory, plan start and end dates, payout frequency, base and super (if applicable) commission rates, prorated and full quotas, and variable payouts per component.

## **Opportunity Splits**

Credit splits are allowed at an opportunity level and can be requested by native-quota carrying salespeople. Opportunity splits must be requested before any work is shared on an opportunity and approved prior to the closed won date.

- Follow [Opportunity Split approval process](/handbook/sales/forecasting#opportunity-splits)
- On approval opportunity split crediting and commissions will be automatically calculated in CaptivateIQ commissions system

## **Sales Representation Letter**

### **Introduction**

As part of SOX 404 control environment all quota carrying reps need to certify if there were any deviations to the handbook policy with respect to quote approval and contract management process. Reps should read and acknowledge the letter and report any exceptions.

### **Goals of certification**

The main aim of this rep certification letter is to identify:

- Any form of communication with a customer that would modify or supersede the terms of an existing subscription and / or a subscription agreement that has not yet been documented or signed.
These arrangements typically result from efforts to: remediate a performance issue, secure future business, obtain a customer reference, retain or improve customer satisfaction, avoid a legal matter or close a deal. Examples of benefits offered to customers could include the following: free or discounted products or services, write-off of a previously recorded account receivable balance, ability of a distributor to re-sell a license to non-intended end-users, extended warranty rights, offer to return products not sold for full refund outside of policies, payment terms that deviate from GitLab’s standard practice or cash refunds.

### **Sales Representation Letter Certifies:**

1. All written or unwritten commitments to customers are documented in the Agreement, Order Form, PO or other written agreement signed by the Customer and authorized signatory at Gitlab. If commitments are not included in customer agreements then they have been documented in a GitLab issue in which the CEO, CFO or CRO has been tagged.
1. All customer contract files (including purchase orders, contracts, letter agreements, sales offers, amendments, and any other correspondence) are complete, properly signed by each party, and the appropriate contract record in Salesforce is filled in so it is easily accessible.
1. All indirect sales (e.g. those through a partner/distributor/ reseller) are supported by a bona fide end-user purchase order or contract.
1. All purchase orders and/or contracts with customers were fully executed by an appropriate customer employee and authorized GitLab employee, if required by local business practice, on or before the delivery date of such contracts.
1. All side arrangements (if any) with customers have been approved by a GitLab executive with authority to make such arrangements and has subsequently been disclosed to the GitLab Revenue Recognition Team (“Revenue Team” controller, senior technical accounting manager, the Principal Accounting Officer, or CFO) in writing.
1. The Company’s customers have not a) been granted any rights to return products; b) receive credits for products or services delivered or receive additional products or services that are not specifically included in the agreements or amendments with such customers, or c) transfer of licenses or delivery of the software to an unauthorized end-user.
1. We did not communicate, directly or indirectly, to any customer that they are free to activate and use the license subscription prior to the subscription start date agreed in the executed order form. Further, we have communicated any knowledge of such unauthorized use to the Finance team.
1. All agreements with unfulfilled performance guarantees or remaining acceptance provisions have been disclosed in the customer agreement or amendments and to the Revenue Team.
1. Multiple agreements with a single customer meeting any of the following criteria have been disclosed to the Revenue Team: agreements were executed within a short time frame (i.e. within 3 months) of each other, the contract terms are interrelated (i.e. payment terms under one agreement are linked to the performance of another), negotiations of the contracts were conducted jointly.
1. The Company has not granted payment terms that deviate from GitLab’s standard practice under any customer sales agreements that are not specifically disclosed in the customer agreement or amendments.
1. All customer arrangements for which payment is contingent on the customer’s obtaining financing from an outside funding source have been disclosed in the customer agreement.
1. There are no known credit memos to be recorded/issued subsequent to the end of the quarter noted above that relate to products or services delivered during the quarter or prior, which would reverse revenue recorded for that arrangement equal to or in excess of 5% of the deal value.
1. Please confirm if there were any changes to quotes after approval or if quotes are sent to a customer and/or not reviewed by Sales operations or Deal desk , please disclose.
1. I personally questioned each of my direct reports (if any) with respect to the representations noted above and am not aware of any deviations not already disclosed.
1. During the Quarter, I have not engaged in any activity, nor have I become aware of any other GitLab employee or employee of any GitLab subsidiary engaging in any activity, which violates the Company’s Code of Business Conduct (/handbook/people-operations/code-of-conduct/), including any activity in violation of the Foreign Corrupt Practices Act - which prohibits any payments to foreign (in jurisdictions outside of the United States) officials for the purpose of obtaining or keeping business.
1. Nothing has come to my attention from previous quarter sales activities that have changed or been disclosed to me that I was not aware of previously, but now am aware of.

### **Sales Rep Certification Process**

Rep letters will be sent quarterly by the Internal Audit Team. Reps are required to acknowledge the certification letter.

### **Sales Rep Certification Process Timeline**

| **Activity** | **Timeline** |
| -------- | -------- |
| Roll out certificates by Internal Audit | 4th day of the subsequent month after the quarter-end |
| Receive responses by | 8th day of the subsequent month after the quarter-end |
| Internal Audit to collate all the information and communicate to Principal Accounting Officer | 12th day of the subsequent month after the quarter-end |
| Principal Accounting Officer and Chief Financial Officer to review the responses and to communicate to audit committee | 14th day of the subsequent month after the quarter-end |

- If any of the timelines above fall under a public holiday then the previous working day will be a due date for the respective activity.

### **Request for Quota Relief**

1. The initial request for quota relief shall be made from the individual to their direct manager with sufficient supporting justification for the request.
1. If approved by their Manager and Departmental VP; the Departmental VP submits the request to sales-comp@gitlab.com for review by Commissions Team and eventually by CRO.
1. Final ruling will be communicated back to Departmental VP with details that may include retroactive adjustments and effective date.

## **FY21 SPIFFS (Sales Performance Incentive Funding Formula)**

1. To promote healthy competitions and drive the right behaviors, Sales leadership could provide a short-term SPIFF (subject to approval via the [Authorization Matrix](/handbook/finance/authorization-matrix/#authorization-matrix))
1. Sales leadership needs to request approvals via an issue that contains the following details:

- Purpose of SPIFF
- Duration of SPIFF
- Criteria to be eligible for SPIFF
- Total # of eligible winners
- Total SPIFF expenses

### **FY21 Professional Services Spiff (v2)**

**Effective 2020-06-15 (replacing the prior SPIFF) [FY21 PS SPIFF](https://docs.google.com/presentation/d/1lPZAmdHPPJwhJ4felwsgDXopIt7Usn3UnK5siOpQMlY/edit#slide=id.g88c7cba71f_10_1)**

##### **PS SPIFF Tracking Process:**

1. Standard Professional Services Deal Process: Follow standard [Professional Services deal process](/handbook/customer-success/professional-services-engineering/selling/)
1. Primary TAM must be identified on the Account record to receive credit
1. Assigning Solution Architect to Opportunity: The following teams can edit the "Primary Solution Architect Field" on the related Opportunity
    - Solution Architects
    - Solution Architect Managers
    - Ops (SalesOps, SalesSystems)
    - CS Leadership
    - _Note: Once the opportunity is closed, the field is no longer editable outside of Sales Support/Sales Ops._
1. Professional Services Compensation:
    - SFDC will capture the PS $ amount in the field named "Comp Pro Serv Value" on the license related opp
    - This will capture both pre-existing SKUs AND associated Professional Services Opportunities for an SOW
1. [SFDC Dashboard](https://gitlab.my.salesforce.com/01Z4M000000slWT) to track PS Open pipe and won deals. Note "Comp Pro Serv Value" on Opportunity will reflect amount considered for compensation.
