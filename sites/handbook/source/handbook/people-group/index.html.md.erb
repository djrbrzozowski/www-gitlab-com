---
layout: handbook-page-toc
title: People Group
description: "The People Group at GitLab, made up of the People Success and Recruiting Departments, supports all GitLab team members through the complete team member lifecycle."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Role of the People Group
{: #role-peopleops}

In general, the People Group teams and processes are here to provide support to our team members; helping make your life easier so that you can focus on your work and contributions to GitLab. On that note, please do not hesitate to [reach out](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group) with questions!

## Departments and Teams within the People Group

The overall People Group is a combination of the _People Success_ and _Recruiting_ Departments.

| Department Name | Encompassed Teams |
| --------------- | ----------------- |
| People Success | Total Rewards; People Operations (Specialists, Experience, Engineering, Compliance, Team Member Relations); People Business Partners; Diversity, Inclusion & Belonging; Learning & Development |
| Recruiting | Candidate Experience; Recruiting; Recruiting Operations and Insights; Sourcing; Talent Brand |

## Need Help?
{: #reach-peopleops}

### In Case of Emergency

Please email: `peopleops@gitlab.com` with "Urgent" in the subject line (we aim to reply within 24 hours, however usually sooner). The [Director, People Operations](https://about.gitlab.com/company/team/#jmitchell) and [Senior Manager, Total Rewards](https://about.gitlab.com/company/team/#brittanyr) are also both contactable 24/7 via their mobile numbers that appears in slack. In the event a team member is in an unsafe situation due to natural disaster, please see the [Disaster Recovery Plan](/handbook/people-group/disaster-recovery-plan/) page.

## How to Report Violations

As stated in the [Code of Business Conduct & Ethics](handbook/people-group/code-of-conduct/) page, we have a section related to [reporting violations](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#viii-questions-reporting-and-effect-of-violations). GitLab has engaged Navex to provide EthicsPoint, its comprehensive and confidential reporting tool, an anonymous ethics hotline for all team members. The purpose of the service is to insure that any team member wishing to submit a report anonymously about conduct addressed in the [Code of Business Conduct & Ethics](handbook/people-group/code-of-conduct/) can do so without the fear of [retribution](/handbook/people-group/people-policy-directory/#commitment-to-non-retaliation).

In addition to EthicsPoint, GitLab has engaged Lighthouse Services to provide an anonymous reporting hotline for all team members to submit reports regarding team member relations. Reports may cover but are not limited to the following topics: wrongful discharge or disciplinary action, [sexual harassment](/handbook/anti-harassment/#sexual-harassment), discrimination, conduct violations, alcohol and substance abuse, threats, improper conduct, violation of company policy.

Please note that the information provided by you may be the basis for an internal and/or external investigation into the issue you are reporting and your anonymity will be protected by EthicsPoint to the extent possible by law. However, your identity may become known during the course of the investigation because of the information you have provided. Reports are submitted by Lighthouse to a company designee for investigation according to our company policies.

Lighthouse has a toll free number and other methods of reporting are available 24 hours a day, 7 days a week for use by team members. 

- Website: [https://www.lighthouse-services.com/gitlab](https://www.lighthouse-services.com/gitlab)
- USA Telephone:
    - English speaking USA and Canada: 833-480-0010
    - Spanish speaking USA and Canada: 800-216-1288
    - French speaking Canada: 855-725-0002
    - Spanish speaking Mexico: 01-800-681-5340
- All other countries telephone: +1-800-603-2869
- E-mail: reports@lighthouse-services.com (must include company name with report)
- Fax: (215) 689-3885 (must include company name with report)

## Welcome

Welcome to the People Group handbook! You should be able to find answers to most of your questions here. You can also check out [pages related to People Group](/handbook/people-group/#other-pages-related-to-people-operations) in the next section below. If you can not find what you are looking for please do the following:

- [**The People Group**](https://gitlab.com/gitlab-com/people-ops) holds several subprojects to organize the people group; please create an issue in the appropriate subproject or `general` if you're not sure. Please use confidential issues for topics that should only be visible to GitLab team-members. Similarly, if your question can be shared please use a public issue. Tag `@gl-peopleops` or `@gl-hiring` so the appropriate team members can follow up.
    - Please note that not all People Group projects can be shared in an issue due to confidentiality. When we cannot be completely transparent, we will share what we can in the issue description, and explain why.
    - [**Employment Issue Tracker**](https://gitlab.com/gitlab-com/people-group/employment-templates): Only Onboarding, Offboarding and Transition Issue Templates are held in this subproject, and they are created by the People Experience Team only. Interview Training Issues, are held in the [Training project](https://gitlab.com/gitlab-com/people-group/Training) and created by the Recruiting team. Please see the [interviewing page](/handbook/hiring/interviewing/#typical-hiring-timeline) for more info.
- [**Chat channel**](https://gitlab.slack.com/archives/peopleops); Please use the `#peopleops` Slack chat channel for questions that do not seem appropriate for the issue tracker. For access requests regarding Google or Slack groups, please create an issue here: https://gitlab.com/gitlab-com/team-member-epics/access-requests. For questions that relate to Payroll and contractor invoices please direct your question to the `#payroll`, `#expense-reporting-inquiries` and `#finance` channel for Carta. Regarding questions for our recruiting team, including questions relating to access, or anything to do with Greenhouse, referrals, interviewing, or interview training please use the `#recruiting` channel. For more urgent general People Group questions, please mention `@peoplegeneral` to get our attention faster.
- If you need to discuss something that is confidential/private (including sensitive matters surrounding your team), you can send an email to the Team Member Relations team: teammemberrelations@gitlab.com. Any team member, regardless of location, can use this alias to discuss a private sensitive matter.
- If you only feel comfortable speaking with one team member, you can ping an individual member of the People Group, as listed on our [Team page](/company/team/).
- If you wonder who's available and/or in what time zone specific team members of the People Group are, you can easily check via Google Calendar or the [GitLab Team Page](https://about.gitlab.com/company/team/)
- If you need help with any technical items, for example, 2FA, please ask in `#it_help`. The channel topic explains how to create an issue. For urgent matters you can mention `@it-ops-team`.

## How to reach the right member of the People Group

This table lists the aliases to use, when you are looking to reach a specific group in the People Group. It will also ensure you get the right attention, from the right team member, faster.

| Subgroup | GitLab handle | Email | Slack Group handle/channel | Greenhouse | Issue tracker |
| -------- | ------------- | ----- | ------------------ | ---------- |
| [People Business Partners](https://gitlab.com/gitlab-com/people-group/General) | @gl-peoplepartners, _Please add the `pbp-attention` label to any issue or MR that requires PBP review, collaboration, or feedback_ | peoplepartners@ gitlab.com | @peoplepartners | n/a | [People Group issue tracker](https://gitlab.com/gitlab-com/people-group/General/-/issues) |
| [Total Rewards](https://gitlab.com/gitlab-com/people-group/total-rewards) | @gl-total-rewards | total-rewards@ gitlab.com | #total-rewards | n/a | [Total Rewards issue tracker](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues) |
| [People Operations Specialists](https://gitlab.com/gitlab-com/people-group/General) | @gl-peopleops | peopleops@ gitlab.com | @peopleops_spec | n/a | [People Group issue tracker](https://gitlab.com/gitlab-com/people-group/General/-/issues) |
| [People Compliance Specialist](https://gitlab.com/gitlab-com/people-group/compliance) | TBA | TBA | TBA | n/a | [Compliance issue tracker](https://gitlab.com/gitlab-com/people-group/compliance/-/issues) |
| [People Experience Associates](https://gitlab.com/gitlab-com/people-group/employment-templates) | @gl-people-exp | people-exp@ gitlab.com | @people_exp | n/a | [People Group issue tracker](https://gitlab.com/gitlab-com/people-group/General/-/issues) |
| Team Member Relations | @atisdale-ext    | teammemberrelations@gitlab.com|   n/a     |   n/a    |  TBA     |
| [People Group Engineering](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-operations-engineering) | No alias, create issues for People Engineering [here](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-operations-engineering/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)| n/a | #peopleops-eng | n/a | [People Operations Engineering issue tracker](https://gitlab.com/gitlab-com/people-group/peopleops-eng/people-operations-engineering/-/issues) |
| [Diversity, Inclusion and Belonging](https://gitlab.com/gitlab-com/diversity-and-inclusion) | No alias yet, @mention the
 [Diversity, Inclusion and Belonging Partner](/job-families/people-ops/diversity-inclusion-partner/) | diversityinclusion@ gitlab.com | n/a | n/a | [Diversity, Inclusion and Belonging issue tracker](https://gitlab.com/gitlab-com/diversity-and-inclusion/-/issues) |
| [Learning and Development](https://gitlab.com/gitlab-com/people-group/Training) | No alias yet, @mention the [Learning and Development Generalist](/job-families/people-group/learning-development-specialist/) | learning@ gitlab.com | n/a | n/a | [Learning and Development issue tracker](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues) |
| Recruiting Leadership | @gl-recruitingleads | n/a | @recruitingleads | n/a | [Recruiting issue tracker](https://gitlab.com/gitlab-com/people-group/recruiting/-/issues) |
| [Recruiting](/company/team/?department=recruiting) | @gl-recruiting | recruiting@ gitlab.com | @recruitingteam | n/a | [Recruiting issue tracker](https://gitlab.com/gitlab-com/people-group/recruiting/-/issues) |
| [Employer Branding](/job-families/people-group/employment-branding-specialist/) | No alias yet, @mention the [Senior Talent Brand Manager](/company/team/#bchurch) | employmentbranding@ gitlab.com | n/a | n/a | [People Group issue tracker](https://gitlab.com/gitlab-com/people-group/General/-/issues) |
| [Candidate Experience Specialist](/job-families/people-group/candidate-experience/) | @gitlab-com/gl-ces | ces@ gitlab.com | @ces | @ces* | [Candidate Experience issue tracker](https://gitlab.com/gitlab-com/people-group/talent_acquisition/candidate-experience/-/issues) |
| [Recruiting Operations and Insights](/job-families/people-ops/recruiting-operations-insights/) | @gl-recruitingops | recruitingops@ gitlab.com | @recruitingops | @recruitingops | [Recruiting issue tracker](https://gitlab.com/gitlab-com/people-group/recruiting/-/issues) |
| [Sourcing](https://about.gitlab.com/job-families/people-ops/recruiting-sourcer/) | @gl-sourcingteam | n/a | @sourcingteam | n/a | [Recruiting issue tracker](https://gitlab.com/gitlab-com/people-group/recruiting/-/issues) |

## Team Calendars

Here's an overview of all the People group's team events.

### To add your event to this table please follow the below instructions:

- Click [here](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/blob/master/-/data/people_group/calendar.yml) to be directed to the .yml file
- Using at Total Reward as a sample please update events that you foresee over the coming months. If you would also like to add past events please do: Eg.
- team: Total Rewards
events:
    - start_date: 2020-02
    name: Audit FY 2021 Annual Compensation Review
- Once you have included your events please assign to your manager for merging

<%= partial "includes/people-operations/calendar_overview" %>

## People Business Partner Alignment to Division

Please reach out to your aligned People Business Partner (PBP) to engage in the following areas:

- Career development/[Promotions](/handbook/people-group/promotions-transfers/)
- [Succession planning](/handbook/people-group/performance-assessments-and-succession-planning/#succession-planning)
- New organizational structures or re-alignment of team members
- Team member relations issues (i.e. performance or behavioral issues) 
- Performance issues (before starting a [PDPs](/handbook/leadership/underperformance/#performance-development-plan-pdp)/[PIPs](/handbook/leadership/underperformance/#performance-improvement-plan-pip))
- [Coaching](/handbook/leadership/coaching/) guidance
- Team member engagement
- Hiring challenges (candidate pipeline, compensation, all other hiring concerns)
- Team member absence or sick leave
- Team member [relocations](/handbook/people-group/relocation/)
- An unbiased third party to discuss issues or problems and possible ideas for next steps

<%= partial "includes/people-operations/people_business_partners" %>

**If you would like People Business Partners' input, collaboration, or feedback on an issue or MR, please add the `pbp-attention` label, which will flag the issue for triaging on our People Business Partner issue board.** See the [People Business Partner Issue Board](/handbook/people-group/index.html#people-business-partner-issue-board) section below for more information on our triaging and lableing process.


### People Business Partner (PBP) Holiday Coverage

In order to ensure the PBP team is able to take time off during the holidays, while simultaneously ensuring we are available for support should urgent items arise, we have put together a PBP holiday coverage on-call plan. While many of us will be on vacation, we will be on an emergency on-call rotation should something urgent arise and reachable via Whatsapp or a text message on our personal cell phones (which can be found in Slack). 

If you have something urgent that has arisen and your [aligned PBP](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) is out of the office, please refer to Holiday Coverage Calendar table below. 

Before proceeding with contacting PBPs in line with the holiday coverage schedule, please consider:

1. Is this urgent?
    * Examples of urgent/time-sensitive requests: [gross misconduct](https://about.gitlab.com/handbook/anti-harassment/#types-of-harassment), a medical emergency, an immediate resignation
    * Examples of non-urgent requests: a dip in performance, approval for a discretionary bonus
2. Is your aligned PBP available?
    * If your [aligned PBP](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) is not on vacation, please reach out to them first.

#### Holiday Coverage Calendar

**As many of the PBPs will also be on vacation and not actively checking email and Slack, please send us a Whatsapp or text message to contact us as opposed to reaching out via Slack or email for timely responses. Our mobile numbers can be found in our Slack profiles.**

If your [aligned PBP](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division) is not on vacation, please reach out to them first. If they are not available, please contact any one of the other available PBPs. 

_Please be mindful of timezone when determining which PBP to reach out to._

| Week | Available PBPs | PBP Timezone
| -------------- | ----------------- | ----------------- |
| December 21 - 25 | Julie Armendariz | CST - GMT-6 (Dallas, TX) |
| December 21 - 25 | Carolyn Bednarz | EST - GMT-5 (Boston, MA) |
| December 21 - 25 | Giuliana Lucchesi | CST - GMT-6 (Chicago, IL) |
| December 28 - January 1 | Lorna Webster | EST - GMT-5 (Atlanta, GA) |
| December 28 - January 1 | Roos Takken | CET - GMT+1 (Amsterdam, Netherlands)


### People Business Partner Issue Board

The People Business Partners (PBPs) have an [issue board](https://gitlab.com/groups/gitlab-com/-/boards/1898818?scope=all&utf8=%E2%9C%93&state=opened) to track collaboration on issues across GitLab.com to keep us efficient and collaborative with other teams.

We utilize labels for triaging amongst the PBPs, and to indicate to other teams the level of priority for the PBPs and the status (doing, done, etc.)

#### Triaging and Labeling Flow

1. `pbp-attention` label is added by team member/group seeking input, collaboration, or feedback on an issue or MR. This will flag the issue for triaging on our People Business Partner issue board. _Note: While MRs will not appear on our board, we ask that the `pbp-attention` label is added regardless for analytics purposes._
1. The PBP team will triage the issue in accordance of PBP priority. Each scoped label will have one PBP DRI, _plus_ the additional number of PBP reviewers indicated depending on the level of priority.
1. The PBP who is the DRI will indicate they are the DRI by tagging themselves in the issue.

- `pbp::low` 1 PBP DRI with 1 additional reviewer
- `pbp::medium` 1 PBP DRI with 2 additional PBP reviewers
- `pbp::high` 1 PBP DRI with 3 additional PBP reviewers (at least one should be at the Director level)

1. If the issue is coming from another team seeking PBP collaboration, the PBP team will add the label of the other team for analytics purposes.

- `pbp-DIB` (Diversity, Inclusion, & Belonging)
- `pbp-LD` (Learning & Development)
- `pbp-TR` (Total Rewards)
- `pbp-legal`
- `pbp-peopleops`
- `pbp-recruiting`
- `pbp-other`

1. The PBP team will add the `pbp-done` label when PBP action is complete.

You may reference the [GitLab.com labels](https://gitlab.com/groups/gitlab-com/-/labels?utf8=%E2%9C%93&subscribed=&search=pbp) for an overview of all labels included in the PBP issue board triaging and labeling process.

## Support provided by Director of Legal, Employment, to the People Group.

The Director of Legal, Employment, collaborates with and provides support to the People Business Partner team in many functional areas.

Email approval from the Director of Legal, Employment is required prior to engagement with external counsel to allow for accurate tracking of costs.

Invoices will be sent to Director of Legal, Employment, for approval with the relevant People Business Partner copied for visibility.

## Support provided by the People Operations Specialist team to the People Business Partner team

All tasks will be assigned via slack in the #pbp-peopleops slack channel. This is a private channel. The People Operations Specialist team, will self-assign a task, within 24 hours and comment in slack on the request to confirm.

## People Experience vs. People Operations Core Responsibilities & Response Timeline

Please note that the source of truth for role responsibilites are the job families for [People Operations Specialist](https://about.gitlab.com/job-families/people-ops/people-operations/) and [People Experience Associate](https://about.gitlab.com/job-families/people-ops/people-experience-associate/). The table below is meant to provide a quick overview of some of the 
core responsibilities for each team.

### People Experience Team

| Responsibility | Response Timeline |
| -------------- | ----------------- |
| [Onboarding](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-processes/) | 4 days prior to start date |
| [Offboarding](https://about.gitlab.com/handbook/people-group/offboarding/offboarding_guidelines/) | Immediate action for involuntary & 24 hours for voluntary |
| [Career Mobility](https://about.gitlab.com/handbook/people-group/promotions-transfers/#career-mobility-issue) | Within 24 hours |
| [Letters of Employment](https://about.gitlab.com/handbook/people-group/frequent-requests/#letter-of-employment) | Within 12 - 24 hours |
| [Employment Verification](https://about.gitlab.com/handbook/people-group/frequent-requests/#employmentverification) | Within 12 - 24 hours |
| [Anniversary Emails](https://about.gitlab.com/handbook/people-group/celebrations/#anniversary-gifts) | Last day of the month |
| Slack and Email Queries | Within 12 - 24 hours |
| [Probation Period Notifications](/handbook/people-group/contracts-probation-periods/#probation-period) | Daily (when applicable) |
| [Evaluate onboarding surveys](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) | As responses are submitted |
| [Flowers and gift ordering](https://about.gitlab.com/handbook/people-group/celebrations/) | Within 24 hours |
| [Determining quarterly winners of the onboarding buddy program](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) | Last week of each quarter |
| [People Experience Shadow Program](https://about.gitlab.com/handbook/people-group/people-experience-shadow-program/) | As requested by other team members |

Other listed processes for the People Experience Team can be found [here](/handbook/people-group/people-experience-team)

### People Specialist Team

| Responsibility | Response Timeline |
| -------------- | ----------------- |
| [Contract Renewals](/handbook/people-group/contracts-probation-periods/#contract-renewals) | 30 days or more prior to team member's renewal date |
| [Exit Interviews](/handbook/people-group/offboarding/#exit-survey) | During voluntary offboarding team member's last week |
| [Hosting of Company Calls](/handbook/people-group/group-conversations/) | Daily on a weekly rotation |
| [Administration of the signing of our Code of Conduct](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#code-of-business-conduct--ethics-acknowledgment-form) | Annually in Feb/March |

### Team Member Relations Specialist

The team member relations specialist is responsible for facilitiating and resolving all people matter related team member issues.   They will also assist in creating and informing team members of policies that are fair and consistent for all GitLab team members. As part of the people group the team member relations specialist works with both the manager and team member in an intermediary function to understand and help resolve workplace issues.  The team member relations specialist can also help coach and advise managers and team members on policies and processes.

Team member relation issues can range from simple coaching questions to serious code of conduct violations.  We classify these different level of team member cases by tiers. Example of issues/cases by tier level:

**Tier 1**

- Coaching support
- Performance Management
- Unprofessional behavior
- Relocation concerns
- Absenteeism

**Tier 2**

- [Job abandonment](/handbook/people-group/people-policy-directory/#job-abandonment)
- Manager/team member conflict resolution
- Team member/team member conflict resolution

**Tier 3**

- Ethic Violations
- Harrassment
- Retaliation
- Any Code of Conduct Violation

All Tier 3 cases will be managed together with Legal and the TMR specialist. 

## Team members who would like to discuss a private matter should send an email requesting support to teammemberrelations@gitlab.com.  Please include the following in your email:

 - Subject Line should be  "New Request for assistance,"
 - Name of Team member requesting support
 - Team members location (example Orlando, Florida or Berlin, Germany)
 - Job title of team member requesting support
 - Division
 - Department
 - Hire date
 - Brief description of type support needed. Example - I am looking for guidance for managing a team members' performance.

The Team Member Relations Specialist will reach out within 24 hours from when the request was sent via email during the business week.  The current team member relations team is listed below:

- US team members relations point of contact is Amy Tisdale (atisdale@gitlab.com)
- Non US team members relations point of contact is Jessica Mitchell (jmitchell@gitlab.com) EMEA based team member relation specialist is expected to start 2021-04-06

For urgent requests that need immediate attention please Slack or email Amy Tisdale (atisdale@gitlab.com)  If you are unable to reach Amy Tisdale, below is the escalation path:

- Jessica Mitchell, Director People Operations @jmitchell 
- Pattie Egan, VP People Operations @pegan
- Wendy Nice Barnes, CPO @wbarnes

An urgent request would be something that needs immediate attention like team members safety or a code of conduct violation.  

### Employment Solutions Team

| Responsibility | Response Timeline |
| -------------- | ----------------- |
| [Relocations](/handbook/people-group/relocation/) | As requested, usually completed 30 days or more prior to team member's relocation date |
| [Country Conversions](/handbook/people-group/employment-solutions/#country-conversions) | As soon as approval is given by CFO. Conversion turnaround time can take from 4-12 weeks depending on a variety of factors. |

## Celebrations
{: #celebrations}

How the GitLab team celebrates work anniversaries and further information about anniversary gifts and birthdays can be found on this [page](/handbook/people-group/celebrations)

## Frequently Requested
{: #frequently-requested}

Please review the [frequently requested section](/handbook/people-group/frequent-requests) of the People Handbook before reaching out to the team. The page includes information on accessing a team directory, requesting a letter of employment, mortgage forms, the companies reference request policy, ordering business cards, and changing your name in GitLab systems.

## Other pages related to the People Group

- [Benefits](/handbook/total-rewards/benefits/)
- [Code of Conduct](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/)
- [Promotions and Transfers](/handbook/people-group/promotions-transfers/)
- [Global Compensation](/handbook/total-rewards/compensation/)
- [Employment Solutions (International Expansion)](/handbook/people-group/employment-solutions/)
- [Contracts, Probation Periods & PIAA](/handbook/people-group/contracts-probation-periods/)
- [Incentives](/handbook/incentives)
- [Hiring process](/handbook/hiring/)
- [Leadership](/handbook/leadership/)
- [Learning & Development](/handbook/people-group/learning-and-development/index.html)
- [Onboarding](/handbook/people-group/general-onboarding/)
- [Offboarding](/handbook/people-group/offboarding/)
- [OKRs](/company/okrs/)
- [People Group Vision](/handbook/people-group/people-group-vision)
- [360 Feedback](/handbook/people-group/360-feedback/)
- [Guidance on Feedback](/handbook/people-group/guidance-on-feedback)
- [Effective & Responsible Communication Guidelines](/handbook/communication/#effective--responsible-communication-guidelines)
- [Effective Listening](/handbook/communication/#effective-listening)
- [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-group/gender-pronouns/)
- [Travel](/handbook/travel/)
- [Underperformance](/handbook/leadership/underperformance)
- [Visas](/handbook/people-group/visas)
- [People Group READMEs](/handbook/people-group/readmes/)
- [Women in Sales Mentorship Pilot Program](/handbook/people-group/women-in-sales-mentorship-pilot-program)
- [People Key Performance Indicators](/handbook/people-group/people-success-performance-indicators/#key-performance-indicators)
- [People Performance Indicators](/handbook/people-group/people-success-performance-indicators/#regular-performance-indicators)

## Leadership Handbook

Our [leadership handbook](/handbook/leadership/) has tools and information for managers here at GitLab.

## Boardroom addresses
{: #addresses}

- For the our mailing addresses, see our [visiting](/company/visiting/) page.
- For the NL office, we use [addpost](https://www.addpost.nl) to scan our mail and send it to a physical address upon request.

## Guidelines for Vendor meetings

These guidelines are for all Vendor meetings (e.g. People Group/Recruiting IT system providers, 3rd party suppliers, benefits providers) to enable us to get the best out of them and make the most of the time allotted:

Guidelines:

1. We request external vendor meetings to use our video conferencing tool so we can quickly join the call and record the session if we need to. Confirm with vendor that they agree we can record the call. The DRI for the vendor meeting will generate the zoom link and share with the vendor.
1. Decide ahead of the meeting who should be invited, i.e. those likely to get the most out of it.
1. Ahead of the meeting, we should agree internal agenda items/requirements/priorities and provide to the external provider.
1. In order to make the best use of time, we wish to avoid team introductions on the call, particularly where there are a number of us attending. We can include a list of attendees with the agenda and give it to the vendor before or after the meeting.
1. When a question or issue is raised on the agenda, if the person who raised it is present they will verbalize it live on the call; or if they are not present, then someone will raise it for them. This is a common GitLab practice.
1. Where possible, we request that the vendor provides their slides / presentation materials and any other related information after the meeting.
1. Do not demo your tool live, create a pre-recorded walk-through of the tool and make it available to GitLab before the meeting so we can ask questions if needed.
1. Be cognizant of using inclusive language.
1. We respectfully request that everyone is mindful of the time available, to help manage the call objectives effectively.

## Reporting potential inaccurate LinkedIn Profiles

GitLab does not actively search for LinkedIn profiles that have inaccurate information about being a GitLab Team member.  However, there are instances where a profile is identified and there is question on whether the person is truly a GitLab team member.  Here is the process for flagging a potentially inaccurate profile and the actions that the People Specialist team will do to confirm and potentially report a profile.

- Team members should first check in Slack and/or on the organizational page to determine if the team member is a current GitLab team member.
- If the team member cannot confirm the profle, team members should reach out to the People Specialist team via the **`@peopleops_spec`** tag in the #peopleops Slack channel with the LinkedIn profile in question.
- The People Operations Specialist will confirm in BambooHR that they are not/have not been a team member.
- The People Operations Specialist will confirm with recruiting to confirm if they have ever been interviewed, made an offer or were declined.
- The People Operations Specialist will reach out to the Community Operations Manager to confirm if they are an active member via slack @community-relations @community team.
- If they are a confirmed member of the community but not a team member, the Community Operations Manager will reach out the profile and ask them to change their profile to match their GitLab involvement. 
- If they are not a current member of the Community and recruiting has confirmed they do not have a profile in Greenhouse, recruiting will create a Do Not Hire profile.
- The People Operations Specialist will report the account to LinkedIn via the [Reporting Inaccurate Information on Another Member's Profile](https://www.linkedin.com/help/linkedin/answer/30200?src=or-search&veh=www.google.com%7Cor-search)


## Sending an E-Card

Another nice way to celebrate someone, express your appreciation or show support to a team member is by creating an e-card and having team members sign the card via HelloSign. Have have fun with this, get creative when making the card using google docs, google slides or a program of your choice. 
An example can be found [here](https://documentcloud.adobe.com/link/track?uri=urn:aaid:scds:US:6034eed9-38e3-440e-bf5a-297905e13f06).
Once you have your card created:

1. Upload the document to either HelloSign or Docusign
1. Once uploaded, assign signatures to those individuals that you would like to sign and then send the request to those individuals
1. When an individual has signed you will get an email notification
1. After you have all of your signatures then download the document from HelloSign
1. Slack or email the card to the recipient

## **Using BambooHR**

We use [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.
We don't have one contact person but can email BambooHR if we want any changes made in the platform. The contact info lives in the Secretarial Vault in 1Password.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action, prepare a communication to the team that is approved by the Chief People Officer.

Team Members have access to their profile in BambooHR and should update any data that is outdated or incorrect. If there is a field that cannot be updated, please reach out to the People Ops Analyst with the change.

The mobile app has less functionality than the full website, and the current version has security issues, so use of the BambooHR mobile app is discouraged. If access to BambooHR is required from a mobile device (such as requests that need to be approved), this should be done through the desktop version of the website or through the mobile device's web browser.

## Using Google Drive

**We are a handbook first organziation and google drive should only be utilized when necessary.**

When using google drive, always save team documents to the appropriate [Shared_Drive].

## Using Culture Amp

Administrative access to CultureAmp will be limited based on role and responsiblity.  There are 3 different levels of access:

- Account Administrator access can manage account setting and team member data, create surveys and has access to all survey results.
- Survey Creator admininstrator access can create, manage and design surveys, select participants and share results.
- Survey data analyst administrator can export raw data from all surveys enabled for raw data extract.

To request access please open an access request with the type of account access requested and the business need.  You can assign the access request to Jessica Mitchell @jmitchell or Beverley Rufener @brufener for review and approval.  The default admin access will be the Survey Creator administration rights unless other access is specified.   Please note that Survey Data Analyst admin rights will not be granted to team members outside of the People group with out permission from the CPO.  

## Using RingCentral

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
want to make changes (for door handling, pick extension 101).
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
the current settings which show all the people and numbers that are alerted when the listed User's
number is dialed.
- Add the new forwarding number, along with a name for the number, and click Save.

## WBSO (R&D tax credit) in the Netherlands

For roles directly relating to Research and Development in the Netherlands, GitLab may be eligible for the [WBSO (R&D Tax Credit)](http://english.rvo.nl/subsidies-programmes/wbso).

### Organizing WBSO

**Applications**

As of 2019 GitLab must submit three applications each year and the deadlines for those are as follows:

1. **31 March 2019**, for the May - August 2019 period (Product Manager for Create Features)
1. **31 August 2019**, for the September - December 2019 period (Product Manager for Gitaly)
1. **30 November 2019**, for the January - April 2020 period (Product Manager for Geo Features)

There is a [translated English version of the application template](https://docs.google.com/document/d/15B1VDL-N-FyLe84mPAMeJnSKjNouaTcNqXeKxfskskg/edit) located in the WBSO folder on the Google Drive. The applications should be completed by a Product Manager, responsible for features or a service within GitLab, who can detail the technical issues that a particular feature will solve. Assistance on completing the application can also be sought from the WBSO consultant (based in the Netherlands). The contact details for the consultant can be found in a secure note in the People Ops 1Password vault called WBSO Consultant. The People Operations Specialist team will assist with co-ordinating this process. It is currently owned by Finance.

**Hour Tracker**

Each year a spreadsheet with the project details and hours logged against the work done on the project(s) will need to be created. This is for the entire year. The current hour tracker is located in the WBSO folder on the Google Drive, and shared only with the developers that need to log their hours (located in the Netherlands), Total Rewards Analysts, Finance and the WBSO consultant. Once the projects have been completed for the year, the WBSO consultant will submit the hours and project details to claim the R&D grant from the [RVO](https://english.rvo.nl/). The WBSO consultant will contact Total Rewards Analysts should they have any queries.

## Women in Sales Mentorship Pilot Program

GitLab launched a Women in Sales Mentorship Pilot for the start Q2 of FY21. If the pilot is considered successful, we will consider expanding the program to other cohorts. For more information on the program and how to apply, visit the [Women In Sales Mentorship Pilot Program](/handbook/people-group/women-in-sales-mentorship-pilot-program) handbook page!

## People Policy Directoy

Follow this link to our [People Policy Directory](/handbook/people-group/people-policy-directory/)
