---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@sabrams](https://gitlab.com/sabrams) | 1 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 2 | 280 |
| [@leipert](https://gitlab.com/leipert) | 3 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 4 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 5 | 140 |
| [@stanhu](https://gitlab.com/stanhu) | 6 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 7 | 100 |
| [@engwan](https://gitlab.com/engwan) | 8 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 9 | 80 |
| [@twk3](https://gitlab.com/twk3) | 10 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 11 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 12 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 13 | 80 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 14 | 80 |
| [@splattael](https://gitlab.com/splattael) | 15 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 16 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 17 | 80 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 18 | 70 |
| [@manojmj](https://gitlab.com/manojmj) | 19 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 20 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 21 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 22 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 23 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 24 | 60 |
| [@ck3g](https://gitlab.com/ck3g) | 25 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 26 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 27 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 28 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 29 | 40 |
| [@cngo](https://gitlab.com/cngo) | 30 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 31 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 32 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 33 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 80 |
| [@smcgivern](https://gitlab.com/smcgivern) | 5 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 6 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@sabrams](https://gitlab.com/sabrams) | 1 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 2 | 280 |
| [@leipert](https://gitlab.com/leipert) | 3 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 4 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 5 | 140 |
| [@stanhu](https://gitlab.com/stanhu) | 6 | 100 |
| [@allison.browne](https://gitlab.com/allison.browne) | 7 | 100 |
| [@engwan](https://gitlab.com/engwan) | 8 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 9 | 80 |
| [@twk3](https://gitlab.com/twk3) | 10 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 11 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 12 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 13 | 80 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 14 | 80 |
| [@splattael](https://gitlab.com/splattael) | 15 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 16 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 17 | 80 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 18 | 70 |
| [@manojmj](https://gitlab.com/manojmj) | 19 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 20 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 21 | 60 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 22 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 23 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 24 | 60 |
| [@ck3g](https://gitlab.com/ck3g) | 25 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 26 | 40 |
| [@kerrizor](https://gitlab.com/kerrizor) | 27 | 40 |
| [@mwoolf](https://gitlab.com/mwoolf) | 28 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 29 | 40 |
| [@cngo](https://gitlab.com/cngo) | 30 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 31 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 32 | 30 |
| [@proglottis](https://gitlab.com/proglottis) | 33 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 80 |
| [@smcgivern](https://gitlab.com/smcgivern) | 5 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 6 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


