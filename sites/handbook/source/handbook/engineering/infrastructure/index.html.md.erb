---
layout: handbook-page-toc
title: "Infrastructure"
description: "The Infrastructure Department is responsible for the availability, reliability, performance, and scalability of GitLab.com and other supporting services"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Mission

The Infrastructure Department enables GitLab (the company) to deliver a single DevOps application, and GitLab SaaS users to focus on generating value for their own businesses by ensuring that we operate an enterprise-grade SaaS platform.

The Infrastructure Department does this by focusing on **availability**, **reliability**, **performance**, and **scalability** efforts.
These responsibilities have cost efficiency as an additional driving force, reinforced by the properly prioritized [**dogfooding**](#dogfooding) efforts.

Many other teams also contribute to the success of the SaaS platform because [GitLab.com is not a role](/company/team/structure/#gitlabcom-isnt-a-role).
However, it is the responsibility of the Infrastructure Department to drive the ongoing evolution of the SaaS platform, enabled by platform observability data.

## Vision

The Infrastructure Department operates a fast, secure, and reliable SaaS platform to which (and with which) [everyone can contribute][contribute].

Integral part of this vision is to:

1. Build a highly performant team of engineers, combining operational and software development experience to influence the best in reliable infrastructure.
1. Work publicly in accordance with our [transparency] value.
1. [Use our own product](#Dogfooding) to prepare, build, deliver work, and support [the company strategy][strategy].
1. Align our [strategy](#strategy) with the industry trends, company direction, and end customer needs.

## FY22 Direction

In FY22 we will work towards accomplishing more of the department vision, especially in support of continuing the successful monthly delivery of product and ongoing reliability improvements for our SaaS customers.
We must also work to enable both currently proposed as well as expected needs to scale our infrastructure not only vertically with the existing GitLab.com service, but horizontally with multiple site implementations.

To progress towards the department vision, we are focusing on:

### 1) Best of class performance

Ensure that the platform can respond to existing and new demands to support future growth, with a better user experience.

1. Complete [the migration](/handbook/engineering/infrastructure/production/kubernetes/gitlab-com/) of all user facing services to Kubernetes platform by 1st of July 2021
1. Stay ahead of the platform growth by becoming great at [projecting service needs](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/148) during FY22.
1. Establish capability to implement and operate multiple GitLab sites during FY22, supported by the [Multi Large Working Group](https://about.gitlab.com/company/team/structure/working-groups/multi-large/), and continued into implementation of new ways to offer our SaaS product.
1. Develop a database sharding solution to further increase future scalability.

### 2) Enterprise SaaS security contribution

1. Collaborate with the Security Department to define and create tools, processes, and audit capabilities for accessing platform environments before end of FY22
1. Collaborate with stage groups to address [top enterprise adoption blockers for GitLab.com](https://about.gitlab.com/direction/enablement/dotcom/#top-customer-successsales-issues) by end of FY22

### 3) Make reliability the platform strength

1. Consistently achieve the target SLA by 1st of July 2021
1. Implement and test disaster recovery capabilities before end of FY22
1. Invest in future reliability through targeted efforts to reduce corrective action backlog, increase velocity of platform OS upgrades, and continue Kubernetes migration.

### 4) Grow and develop leadership

1. Enrich the role description, expectations, and impact of Staff Engineers throughout the year
1. Establish a roadmap during 1H FY22 for oncall experience and a continuation of exceptional results in light of team growth and ability utilize additional specialization
1. Create improved clarity of ownership and established DRIs for parts of company infrastructure which still retain some ambiguity (such as vendor and customer)
1. Enable team leaders to effectively organise and lead team projects by 31st of March 2021

### 5) Generate contributions external to the department

1. Increase visibility of Infrastructure department public projects by EOFY22.
1. Generate one or more contributions per month to any of the Infrastructure department projects by EOFY22

---

The direction is accomplished by using _Objectives and Key Results_ (OKRs) as *[quarterly goals](/company/okrs/), aligned throughout the organization*.

Initiatives driven within the Infrastructure Department, often spanning multiple quarters, are represented on the [Infrastructure Department epic](https://gitlab.com/groups/gitlab-com/-/epics/1049).

Other strategic initiatives to achieve this vision are driven by the needs of enterprise customers looking to adopt GitLab.com. [The GitLab.com strategy](https://about.gitlab.com/direction/enablement/dotcom/) catalogs top customer requests for the SaaS offering and outlines strategic initiatves across both Infrastructure and Stage Groups needed to address these gaps.

## Design

The [**Infrastructure Library**][library] contains documents that outline our thinking about the problems we are solving and represents the ***current state*** for any topic, playing a significant role in how we produce technical solutions to meet the challenges we face.

**Blueprints** scope out our initial thinking about specific problems and issues we are working on. **Designs** outline the specific architecture and implementation.

## Dogfooding

The Infrastructure department uses GitLab and GitLab features extensively as the main tool for operating many [environments](/handbook/engineering/infrastructure/environments/), including GitLab.com.

We follow the same [dogfooding process](/handbook/engineering/#dogfooding) as part of the Engineering function, while keeping the [department mission statement](#mission) as the primary prioritization driver. The prioritization process is aligned to [the Engineering function level prioritization process](/handbook/engineering/#prioritizing-technical-decisions) which defines where the priority of dogfooding lies with regards to other technical decisions the Infrastructure department makes.

When we consider building tools to help us operate GitLab.com, we follow the [`5x rule`](/handbook/product/product-processes/#dogfooding-process) to determine whether to build the tool as a feature in GitLab or outside of GitLab. To track Infrastructure's contributions back into the GitLab product, we tag those issues with the appropriate [Dogfooding](https://gitlab.com/groups/gitlab-com/-/labels?utf8=%E2%9C%93&subscribed=&search=dogfooding) label.

## Handbook use at the Infrastructure department

At GitLab, we have a [handbook first policy](/handbook/handbook-usage/#why-handbook-first). It is how we communicate process changes, and how we build up a single source of truth for work that is being delivered every day.

The [handbook usage page guide](/handbook/handbook-usage/) lists a number of general tips. Highlighting the ones that can be encountered most frequently in the Infrastructure department:

1. The wider community can benefit from training materials, architectural diagrams, technical documentation, and how-to documentation. A good place for this detailed information is in the related project documentation. A handbook page can contain a high level overview, and link to more in-depth information placed in the project documentation.
1. Think about the audience consuming the material in the handbook. A detailed run through of a GitLab.com operational runbook in the handbook might provide information that is not applicable to self-managed users, potentially causing confusion. Additionally, the handbook is not a go-to place for operational information, and grouping operational information together in a single place while explaining the general context with links as a reference will increase visibility.
1. Ensure that the handbook pages are easy to consume. Checklists, onboarding, repeatable tasks should be either automated or created in a form of template that can be linked from the handbook.
1. The handbook is the process. The handbook describes our principles, and our epics and issues are our principles put into practise.

## Projects

Classification of the Infrastructure department projects is described on the [infrastructure department projects page](/handbook/engineering/infrastructure/projects).

The [infrastructure issue tracker](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues) is the backlog and a catch-all project for the infrastructure teams and tracks the work our [teams](/teams) are doing–unrelated to an ongoing change or incident.

## How to engage Infrastructure to add a new service

Adding a new service involves work from a number of Infrastructure teams to make sure the service is deployed and operated safely. To help new service deployments run smoothly please open a request issue in the [infrastructure issue tracker](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/new?issuable_template=request-gitlab-com)

## Teams

The Infrastructure Department is comprised of three distinct groups:

* [**Reliability** teams](/handbook/engineering/infrastructure/team/reliability/), which operate all user-facing GitLab services.
* [**Delivery**](/handbook/engineering/infrastructure/team/delivery/), which focuses on GitLab's delivery of software releases to GitLab.com and the public at large.
* [**Scalability**](/handbook/engineering/infrastructure/team/scalability/), which focuses on improving GitLab application at GitLab.com scale.

Product Management duties for the Infrastructure Department are handled by the [Infrastructure PM](/handbook/engineering/infrastructure/product-management/), who reports into the Enablement Stage.

For details on the Department's structure, see the [**Infrastructure Teams Handbook section**](/handbook/engineering/infrastructure/team/).

## Meetings

GitLab is a widely distributed company, and we aim to work asynchronously most of the time. There are times, however, when we must get *together* to discuss topics in real time, and thus, we do have some meetings scheduled. Meetings start on time and end on time&mdash;or earlier.

Our team [calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV9vamk2ZGtpMWZyYzhnOHFxOWZldXUxanRkMEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t) is shared with the company.

Every scheduled team meeting _must_ have a Google Doc agenda attached to the invite. The agendas should be long-running and organized by date. Each meeting's agenda is set and reviewed before the start of each meeting. Everyone invited to the meeting should have edit rights to add agenda items before the start of the meeting and to take notes during the meeting.

##### GitLab SaaS Infrastructure

The GitLab SaaS Infrastructure meeting is one of many channels to share and distribute Infrastructure-related information relevant to the entire company. The meeting is organized by the Infrastructure leadership team and the VP of Infrastructure. This meeting aims to bring together many different related aspects which all influence the GitLab SaaS infrastructure reliability, scalability, performance, and efficiency.

This meeting is always to be recorded and made available in support of those in varying timezones. It is meant to be a learning environment for what is coming, what we've done well, and what we can do to improve. All GitLab team members are welcome to attend this meeting.

While [the agenda](https://docs.google.com/document/d/1wMokFji42Jw4ImfoiQG6dSkHFIWmLVj5Kz6bO3mF948/edit) continually evolves, the main structure is outlined below. Everyone should feel welcome to contribute to the agenda, but [Infrastructure mStaff](/handbook/engineering/infrastructure#mstaff) will work to curate the agenda into the best use of 50 minutes each week.
* Core Topics
   * Availability:  Update on recent week performance, or look back at past month.
   * Recent severity::1/severity::2 Incident Summary: Quick overview to bring attention to recent critical service impacts.
   * Infrastructure related Development escalations: Quick status on [infradev](https://gitlab.com/groups/gitlab-org/-/boards/1193197?label_name[]=infradev) board, triage Open issues if needed.
   * Spend Efficiency: Update on recent performance, quick highlight of important current work.
* Saturation and Capacity
   * Saturation dashboard view
   * Rotating deep dive on specific part of infrastructure saturation or capacity projections
* Wins / Misses / Stories: Highlight key areas where we can celebrate and/or learn.
* Rotating Topics
   * Corrective Action item health: Review of how we're addressing the corrective action backlog [Monthly]
   * Product Limits activity update: Look at how we're progressing in adding and improving useful application limits [Monthly]
   * Key Customer insights: Gain understanding of how customers are using us, learn about key customers [Varies]
   * Interesting project or feature highlight [Varies]

The recording of this meeting is automatically uploaded to [Infrastructure Google Drive](https://drive.google.com/drive/folders/1d_877fGqaKx8DrgPuulOXb-i4RjYanTr).

##### Design and Automation (DNA)

Design and Automation (DNA) is a **purely technical meeting** for Infrastructure ICs to discuss technical topics. The agenda is driven by:
1. Issues that are proposing Firedrill exercises
2. Sharing of technical knowledge (demos or overviews) from Corrective Actions from Incident Reviews.
3. Blueprints, Designs or Readiness reviews from the [readiness repo](https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/library)

Project status discussions are strictly out of bounds in this (the only exception being the resolution of technical dependencies).

While open discussions are welcome, it is strongly recommended that blueprints and designs are used as the source of agenda items. This allows everyone gain the required context–before the meeting starts–for an engaging conversation.

During discussions, it is ok to point shortcomings for a given design. This is one way in which we expand our angle of vision and learn. In general, however, **make it a point to provide alternatives**.

##### Staff/Newsletter Meetings

We have weekly infrastructure oncall handover and staff meetings. These meetings are organized by Infrastructure Managers that occur as a time for SREs to have weekly handover notes for Oncall and other announcements for the team.  We run these meetings from the [Team Newsletter issues](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues?label_name%5B%5D=Reliability-Team-Newsletter).

##### mStaff

Infrastructure mStaff is a loose denomination for the group of people who report directly to the Vice President of Infrastructure. This is a group composed of both managers and individual contributors, and they are responsible for the overall direction of Infrastructure and the achievement of our goals:

<%= direct_team(manager_role: 'VP of Infrastructure')%>

The weekly mStaff brings together Infrastructure's management team for a weekly sync to prepare for the week and address issues that require attention. The meeting is organized by the VP of Infrastructure.

<%= partial "handbook/engineering/infrastructure/_common_links.html" %>

[library]: https://gitlab.com/gitlab-com/gl-infra/readiness/-/tree/master/library

[strategy]: /company/strategy/
[transparency]: /handbook/values/#transparency
[contribute]: /company/strategy/#everyone-can-contribute
