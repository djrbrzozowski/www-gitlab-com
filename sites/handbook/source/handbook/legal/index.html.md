---
layout: handbook-page-toc
title: "The GitLab Legal Team"
description: "Meet the GitLab Legal Team and learn how to engage with us for internal needs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
# Welcome to GitLab Legal!

We are glad you are here! GitLab's [LegalTeam](https://about.gitlab.com/company/team/?department=legal) is small but mighty and entirely at your service. 

## How to Partner With the GitLab Legal Team to get What you Need 
Please take a moment to read through the below, where we’ve tried to address the best ways to engage GitLab Legal depending upon your specific request. Alternatively, please feel free to check out this [tutorial video](https://www.youtube.com/watch?v=Kh_pPeJkF2o), featuring our very own Rob Nalen. If you still have questions, you can always reach us at *[`#legal`](https://gitlab.slack.com/archives/legal)*

### Anonymous Internal Ethics and Compliance Reporting

We take employee concerns very seriously and encourage all GitLab Team Members to report any ethics and/or compliance violations by using EthicsPoint. Futher details are found on the [Code of Business Conduct and Ethics](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#viii-questions-reporting-and-effect-of-violations) handbook page. We have also engaged Lighthouse to support when relating to reporting team member relations issues. Details can be found on the [People Group](/handbook/people-group/#how-to-report-violations) handbook page.

### Quick Questions
For quick questions that **_do not_** require legal advice, deliverables, or any discussion of confidential information, you can reach out to the GitLab Legal Team at *[`#legal`](https://gitlab.slack.com/archives/legal)*. We find this channel best for questions regarding process, who handles what or how to find certain things if the Handbook has not yielded the right result for you after searching. *[`#legal`](https://gitlab.slack.com/archives/legal)* is not a private channel, so your inquiry will be visible to the entire company. One of our Team Members will do their best to answer your question in a timely fashion. If your request is for legal advice, deliverables, or any discussion of confidential information, please keep reading. 

### Privileged / Confidential Communications
If you have a request that involves confidential and/or sensitive information, please email [legal@gitlab.com](mailto:legal@gitlab.com).  

For more information on Attorney-Client Privilege, see the [General Legal FAQs](https://about.gitlab.com/handbook/legal/#general-legal-faqs) below. 

### Sales Guide: Collaborating with GitLab legal 
**Are you a member of GitLab Sales?** Everything you're looking for can be found in the [Sales Guide: Collaborating with GitLab Legal](https://about.gitlab.com/handbook/legal/customer-negotiations/)

### Marketing Guide: Collaborating with GitLab legal 
**Is your need marketing related?** Everything you're looking for can be found in the [Marketing Guide: Collaborating with GitLab Legal](https://about.gitlab.com/handbook/legal/marketing-collaboration/)

### Requests with Deliverables
If you are making a request that requires some sort of deliverable, please use the list below to determine how you should reach out. If you are unsure where your non-Slack request fits, refer #3 below. 

### 1. Vendor, Partner, and other "To Pay" Contracts
* If you are looking for a new vendor, need an NDA for a vendor , or need review of a vendor  contract, these services are handled by the Procurement Team. For purposes of this process, anyone that will receive payment from GitLab is considered a vendor. 
* Legal will be brought in by Procurement for escalations only. Please see the [Procurement Page](https://about.gitlab.com/handbook/finance/procurement/) for more information on the Vendor Management Process. Once a Vendor NDA and/or Contract has been completed, it should be uploaded by the requestor into our contract management database tool [ContractWorks](https://about.gitlab.com/handbook/legal/vendor-contract-filing-process/). If you need a license, you will need to submit an Access Request.

### 2. Insurance Certificate
* If you need an insurance certificate (other than for worker's compensation) you can send an email request directly to our insurance broker at *[ABD](mailto:abdcertdept@theabdteam.com).*    You will need to include contact information for the customer seeking to be added to the certificate and any other specific requirements relating to the coverage. If you require an insurance certificate for worker's compensation email: payroll@gitlab.com with the same information.

* For a summary of GitLab's insurance coverage [ please refer to this link](https://drive.google.com/file/d/17pvhu9cKtoQatO7xOlKbwSoOXDEJRpiQ/view?usp=sharing).

### 3. Other Legal Requests 
We use [issues](https://gitlab.com/gitlab-com/legal-and-compliance/issues) to track all other requests that are not customer-related, but require input from the Legal Team or a deliverable, such as: 

- A legal question on an existing issue
- Open source questions
- General compliance inquiries 
- [NDAs](https://drive.google.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view)
- Waivers 
- [Media Consent and Release Forms](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)

Please tag the relevant Legal Team Member and ask your questions in the issue that relates to your question. If there is not an issue related to your question, please create an issue in the [Legal and Compliance Issue Tracker](https://gitlab.com/gitlab-com/legal-and-compliance/issues) using issue templates. **All Legal and Compliance issues should be marked as confidential. GitLab team members will be able to access these issues directly.** For more specific instructions, see our [issue tracker workflow](https://about.gitlab.com/handbook/legal/issue-tracker-workflows/).

**_If you are unsure where your request fits, an issue is where your journey begins._**  
Please be sure to include sufficient detail regarding your request, including: time-sensitive deadlines, relevant documents, and background information necessary to respond.
 
## Contract Templates
* [Mutual Non-Disclosure Agreement](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing)
* [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
* [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)

## Other Resources
* [Authorization Matrix](https://about.gitlab.com/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents
* [Requirements for Closing Deals](https://about.gitlab.com/handbook/business-ops/order-processing/#step-7--submitting-an-opportunity-for-approval) - requirements for closing sales deals
* [Terms](https://about.gitlab.com/terms/) - legal terms governing the use of GitLab's website, products, and services
* [Acceptable Licenses](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/licensing.md) - list of acceptable licenses covering third party components used in development

### Processes
* [Uploading Third Party Contracts to ContractWorks](https://about.gitlab.com/handbook/legal/vendor-contract-filing-process/)
* [Issue Tracker Workflows](https://about.gitlab.com/handbook/legal/issue-tracker-workflows/)
* [Privacy Review Process](https://about.gitlab.com/handbook/legal/privacy/#privacy-review-process)

## Policies
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [Compliance](https://about.gitlab.com/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Employee Privacy Policy](https://about.gitlab.com/handbook/legal/privacy/employee-privacy-policy/) GitLab's policy for how it handles personal information about team members.
* [General Guidelines](https://about.gitlab.com/handbook/general-guidelines/) - general company guidelines
* [GitLab Modern Slavery Act Transparency Statement](https://about.gitlab.com/handbook/legal/modern-slavery-act-transparency-statement/)
* [Privacy Policy](https://about.gitlab.com/privacy/) - GitLab's policy for how it handles personal information
* [Records Retention Policy](https://about.gitlab.com/handbook/legal/record-retention-policy/) - GitLab's policy on the implementation of procedures, best practices, and tools to promote consistent life cycle management of GitLab records
* [Related Party Transactions Policy](https://about.gitlab.com/handbook/legal/gitlab-related-party-transactions-policy/)
* [Trademark](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-guidelines/#trademark) - information regarding the usage of GitLab's trademark

# Code of Business Conduct and Ethics 
GitLab's Code of Business Conduct and Ethics summarizes the ethical standards for all directors, officers, team members and contractors of GitLab and of its direct and indirect subsidiaries and is a reminder of the seriousness of our commitment to our values. [Please click here to read GitLab's Code of Business Conduct and Ethics in its entirety.](https://about.gitlab.com/handbook/legal/gitlab-code-of-business-conduct-and-ethics/) 

# General Legal FAQs

## 1. The U.S. Attorney-Client Relationship 
This discussion is limited to the practice of law in the U.S. As we continue to grow globally we will update this and expand how privilege applies in other jurisdictions.

### What is Attorney-Client Privilege?
Attorney-Client Privilege is a principle that provides protections for certain communications between clients and their attorneys that meet specific criteria. 

First of all, the communications must be for the purpose of seeking legal guidance and advice. For this reason, the underlying facts may not be protected if they are available from another source. Opinions and analysis of the facts, and discussions thereof, with the attorney are protected. 

Secondly, communications must be confidential. Information is also not protected if it is available from another source meaning that simply telling your attorney or copying your attorney on a communication does not protect the information. 

### What is Work Product Privilege?
Work Product is a U.S. doctrine in which an attorney’s notes, observations, thoughts, and research prepared by, or at the direction of, an attorney in anticipation of litigation are protected from being discoverable during the litigation process.

### What is the purpose of these Privileges?
Attorney-Client and Work Product privileges allow clients to speak freely with their attorneys and encourage full disclosure so they can receive accurate and competent legal advice without the fear of having their attorney compelled to testify against them and disclose the information shared by the client.

### Who do these Privileges Apply to at GitLab?
There is not one uniform answer that covers all jurisdictions in the U.S. However, most jurisdictions will use at least one of the following tests to see if the individuals involved have privileged communication.

- The **Control Group Test**. This test is quite restrictive and only allows for the protection of corporate communications to the corporation's controlling executives and managers. This test cannot be used in federal courts, but is still used in some states.
- The **Subject Matter Test**. Instead of looking solely at the roles of the employees involved, this test looks at the subject matter of the employees’ communications. If an employee has been directed by a supervisor to discuss a subject matter that relates to the employees job with an attorney, this may be covered by subject matter privilege.
- The **Upjohn Test**. This modified version of the Subject Matter Test requires additional criteria to be met. In addition to the subject matter being relevant to the employee’s duties, the employee must also have awareness and intent concerning the legal advice being sought and/or given.
- The **Upjohn Warning**. A company’s attorney does not represent an employee individually, but instead represents the interests of the company. A company can waive its privilege at any time, meaning the company could choose to disclose information the attorney received from a covered employee in confidence for use as evidence in a legal proceeding in order to protect the company from liability.
 
### How do you Protect the Privileges that Apply to You When Seeking Legal Advice?
- Direct the communication to a practicing licensed attorney. Privilege does not apply to other non-attorney members of the legal team.
- It is best practice to have privileged conversations with the attorney via Zoom.
- If other individuals will need to participate in the discussion, consult with the attorney, and only include the minimum necessary individuals in the conversation, in other words, keep the circle of trust small.
- If it is necessary to communicate by email, in the “Subject” line, and at the top of the body of the communication, include the phrase “AC PRIV”.
- Do not overuse the claim of privilege. Limit its use to when actually seeking legal guidance and advice and not on any and every correspondence with the attorney.
- You must keep the information discussed confidential, and not share it with anyone outside the circle of trust without first consulting with the attorney.
- Do not forward protected emails to anyone outside the circle of trust.
- Do not copy anyone outside the circle of trust on emails with the attorney.

For more questions and answers about Attorney-Client Privilege in the corporate setting, search “AC Priv tests” in Drive.

## 2. Litigation Holds

### What is a Litigation Hold?

A litigation hold is the process a company uses to preserve all forms of relevant evidence, whether it be emails, instant messages, physical documents, handwritten or typed notes, voicemails, raw data, backup tapes, and any other type of information that could be relevant to pending or imminent litigation or when litigation is reasonably anticipated.  Litigation holds are imperative in preventing spoliation (destruction, deletion, or alteration) of evidence which can have a severely negative impact on the company's case, including leading to sanctions.

Once the company becomes aware of potential litigation, the company's attorney will provide notice to the impacted employees, instructing them not to delete or destroy any information relating to the subject matter of the litigation.  The litigation hold applies to paper and electronic documents. During a litigation hold, all retention policies must be overridden.

## 3. Foreign Corrupt Practices Act

The Foreign Corrupt Practices Act is a United States federal law that prohibits U.S. citizens and entities from bribing foreign government officials to benefit their business interests. It is not only an invaluable tool to help fight corruption but one to which we must be compliant. As GitLab Inc. is a U.S. incorporated entity, we need to make sure our operations worldwide are compliant with the provisions of the Foreign Corrupt Practices Act. To that end, GitLab requires Team Members to complete an annual online course relating to anti-bribery and corruption at GitLab. In the training, learners will explore improper payments, including facilitation payments and personal safety payments, as well as policies on commercial bribery. 

The goal of the course is to ensure our Team Members understand what it takes to avoid corruption, especially in high-risk countries, and to ensure GitLab is compliant with legal and regulatory obligations. 
