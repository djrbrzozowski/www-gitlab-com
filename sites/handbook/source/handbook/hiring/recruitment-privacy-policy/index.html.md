---
layout: handbook-page-toc
title: "Recruiting Privacy Policy"
description: "This Recruitment Privacy Policy describes GitLab Inc. and its affiliates ("GitLab") practices for collecting, storing, and processing your personal information as part of the recruitment process."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruitment Privacy Policy

This Recruitment Privacy Policy describes GitLab Inc. and its affiliates (“GitLab”) practices for collecting, storing, and processing your personal information as part of the recruitment process.

#### What Personal Information Does GitLab Collect About You?

*Information You Provide Directly*

We may collect the following personal information from you when you apply for a position:
* Contact Information such as name, email address, physical address, telephone number;
* Employment Background Information such as resume/CV, employment history, academic and professional qualifications;
* Demographic Information such as racial or ethnic origin for government reported statistics when permitted by law or with your consent.

*Information We Collect from Other Parties*

We may collect personal information from other parties as part of the recruitment process. For example:
* Information provided from agencies performing background checks;
* Information from public sources and social media sites;
* Information from recruitment agencies that you have asked to support you in finding a job;
* Information provided by GitLab team members who have referred you for a job.

#### How does GitLab Use Your Personal Information?

We may collect and use your personal information for the following purposes:
* To communicate with you about your job application and potential future job opportunities;
* To administer, support, and manage the job application process;
* Where necessary to comply with applicable legal or regulatory requirements;
* To evaluate and report the demographic makeup of our company where allowed by law; and
* For other purposes as described to you at the time we collect your personal information.

#### When Does GitLab Share Your Information and Who are the Recipients?

As part of the recruitment process, GitLab may disclose your personal information in the following circumstances:
* Internally with GitLab team members, contractors, and vendors who require the information to administer and manage the recruitment process;
* To third-party service providers which process personal information on our behalf to provide certain services, such as background checks;
* To third-parties with whom you instruct GitLab to share your personal information;
* We may share your personal information if we believe it is reasonably necessary to comply with valid legal process (e.g., subpoenas, warrants) or protect the rights, property, or safety, of GitLab, our employees, or users;
* If GitLab is acquired or transferred (including in connection with a bankruptcy or similar proceedings), we may share your personal information with the acquiring or receiving entity. 

#### Legal Basis for Processing Your Personal Information

If you are located in the European Economic Area (EEA), United Kingdom, or Switzerland, we collect and process your personal information on the following legal bases set out by applicable law:

*Consent:* We may ask you for your consent to process your personal information. You can withdraw your consent at any time, which will not affect the lawfulness of the processing before your consent was withdrawn. 

*Legitimate Interest:* We process certain personal information for our legitimate interests. These legitimate interests include, for example, administering the recruitment process and managing applicants effectively.

*Performance of a Contract:* We process personal information to perform steps that are necessary prior to entering an employment contract with you, where you are considered for employment.'

*Compliance with Legal Obligations:* In some cases, we may have a legal obligation to process your personal information, such as to meet our legal requirements or in response to a court or regulatory order. We also may need to process your personal information to protect vital interests, or to exercise, establish, or defend legal claims.

#### International Data Transfer

Your personal information may be processed in the United States, the country where you have applied for a job, or any other country where GitLab or its subcontractors have team members or operations.

GitLab may transfer, store, or process your personal information in a country outside your jurisdiction, including countries outside the European Economic Area (“EEA”), Switzerland, and the United Kingdom. If we transfer personal information from the EEA, Switzerland, or United Kingdom to a country outside it, such as the United States, we will enter into EU standard contractual clauses with the data importer, or take other measures to provide an adequate level of data protection.

#### Security

Access to your personal information is controlled and GitLab maintains a comprehensive information security program using administrative, physical, and technical safeguards. 

#### Data Retention

Your personal information will be retained until the end of the employment application process plus a reasonable period thereafter. If your job application is successful, the personal information collected during the recruitment process will be added to your human resources file and retained for the duration of your employment.

#### Your Rights and Choices

You may access, correct, or delete your personal information at any time. You may also withdraw your consent, or object to the processing of your personal information when based on our legitimate interests. To do so, please contact us at: `recruiting@gitlab.com`.    

#### Policy Changes

GitLab may change this Privacy Policy from time to time. If we decide to make a significant change to this Privacy Policy, we will post a notice of the update on this site. We encourage you to periodically review this page for the latest information.

#### Contact Information

If you have questions or concerns about the way we are handling your information, please email us with the subject line "Privacy Concern" at `DPO@gitlab.com`.

The controller of your personal information is the GitLab affiliate who is hiring.

#### Policy Updates

This policy was last updated on `2021-03-24`.

