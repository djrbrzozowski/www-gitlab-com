---
layout: handbook-page-toc
title: "Technical Account Management Handbook"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Handbook Directory

### TAM Segments & Metrics

- [TAM Segments and Associated Metrics](/handbook/customer-success/tam/customer-segments-and-metrics/)
- [TAM Team Metrics Overview (VIDEO)](https://www.youtube.com/watch?v=9b8VviLG3yE&t=2s)

### TAM Responsibilities

- [TAM Skills Matrix](https://docs.google.com/spreadsheets/d/1_UEke64Qkz8wSyqfr_E9qqeAF6rX77w4vIH84Ckm_ts/edit?usp=sharing) (Gitlab Internal)
- [Using Gainsight](/handbook/customer-success/tam/gainsight/)
- [TAM Responsibilities and Services](/handbook/customer-success/tam/services/)
- [TAM and Product Interaction](/handbook/customer-success/tam/product/)
- [TAM and Support Interaction](/handbook/customer-success/tam/support/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
  *  [Infrastructure Escalation & Incident Process](https://about.gitlab.com/handbook/customer-success/tam/escalations/infrastructure/)
- [TAM-to-TAM Account Handoff](/handbook/customer-success/tam/account-handoff/)
- [TAM READMEs](/handbook/customer-success/tam/readmes/) (Optional)

### Customer Journey

##### TAM-Assigned:

- [Account Engagement and Prioritization](/handbook/customer-success/tam/engagement/)
   - [Non-Engaged Customer Strategies](/handbook/customer-success/tam/engagement/Non-engaged-customer-strategies/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Success Plans](/handbook/customer-success/tam/success-plans/)
   - [Developer & Productivity Metrics](/handbook/customer-success/tam/metrics/)
   - [Sample Questions & Techniques for Getting to Good Customer Metrics](https://about.gitlab.com/handbook/customer-success/tam/success-plans/questions-techniques/)
- [Stage Enablement & Stage Expansion - The Two Stage Adoption Motions](/handbook/customer-success/tam/stage-enablement-and-expansion/)
   - [Stage Adoption Metrics](/handbook/customer-success/tam/stage-adoption/)
   - [Tracking Adoption with Usage Ping](/handbook/customer-success/tam/usage-ping-faq/)
- [Cadence Calls](/handbook/customer-success/tam/cadence-calls/)
- [Customer Health Assessment and Risk Triage](/handbook/customer-success/tam/health-score-triage/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)
- [Product Usage Data](/handbook/customer-success/product-usage-data/using-product-usage-data-in-gainsight/)
- [Customer Use Case Adoptiom](/handbook/customer-success/product-usage-data/use-case-adoption/)


##### Digital Journey:

- [Digital Journey](/handbook/customer-success/tam/digital-journey/)
  * [Nominating Contacts for the Digital Journey](/handbook/customer-success/tam/digital-journey/nominating-contacts-for-the-digital-journey/)


### TAM Managers

- [TAM Manager Processes](/handbook/customer-success/tam/tam-manager/)
- [TAM Manager QBR Template](https://docs.google.com/presentation/d/1P7Cao5xgILSSrpEGy7Sh09djilnbbIK91IuTs-Xq7mY/edit?usp=sharing) (Gitlab Internal)
- [TAM Promotion Template](https://docs.google.com/document/d/1Hg16QVYB2qm0gUGR6H_NPz0cd8OGEuvi3GyDtOsEagY/edit?usp=sharing) (Gitlab Internal)


- - -

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, Professional Services Engineers and support.

See the [Technical Account Manager role description](/job-families/sales/technical-account-manager/) for further information.

### Advocacy

A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product. In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business unit's how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption and training.


## Responsibilities and Services

#### Book of Business

Technical Account Managers are responsible for managing a portfolio of customer accounts based on the sum of the annual contract value of the accounts under management. The current target per TAM for the [TAM-assigned segment](/handbook/customer-success/tam/customer-segments-and-metrics/#tam-assigned-segment) is 3.6M per TAM

The team continues to drive efficiency improvements to allow TAMs to effectively manage a larger book of business.

There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilisation of GitLab's products and services. These services include, but are not limited to:

## Relationship Management

- Regular [cadence calls](/handbook/customer-success/tam/cadence-calls)
- Regular open issue reviews and issue escalations
- Account [health assessment](/handbook/customer-success/tam/health-score-triage/)
- [Executive business reviews](/handbook/customer-success/tam/ebr)
- Success strategy roadmaps - beginning with a 30/60/90 day success plan
- To act as a key point of contact for guidance, advice and as a liaison between the customer and other GitLab teams
- Own, manage, and deliver the customer onboarding experience
- Help GitLab's customers realize the value of their investment in GitLab
- GitLab Days

## Training

- Identification of pain points and training required
- Coordination of demo sessions, potentially delivered by the Technical Account Manager or Solution Architect if time and technical knowledge allows
- "Brown Bag" sessions
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

## Support

- Upgrade planning assistance in conjunction with Support
- User adoption strategy
- Migration strategy and planning in conjunction with Support
- Launch support
- Monitors support tickets and ensures that the customer receives the appropriate support levels
- Support ticket escalations

## TAM Tools

The following articulates where collaboration and customer management is owned:

1. [**Account Management Projects**](https://gitlab.com/gitlab-com/account-management): Shared project between GitLab team members and customer. Used to prioritize/plan work with customer.
1. [**Google Drive**](https://drive.google.com/drive/u/0/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U): Internal. Used to capture call notes and other customer related documents.
1. [**Chorus**](/handbook/business-ops/tech-stack/#chorus): Internal. Used to record Zoom calls.
1. [**Gainsight**](/handbook/customer-success/tam/gainsight/): Internal. Used to track customer health score, logging [customer activity](/handbook/customer-success/tam/gainsight/timeline/#activity-types) (i.e. calls, emails, meetings)

### Education and Enablement

As a Technical Account Manager, it is important to be continuously learning more about our product and related industry topics. The [education and enablement handbook page](/handbook/customer-success/education-enablement) provides a dashboard of aggregated resources that we encourage you to use to get up to speed.

## SFDC useful reports 

### Tracking oportunities for your assigned SALs

To ensure that opportunities are listed with the correct Order Type, [this Salesforce report](https://gitlab.my.salesforce.com/00O4M000004agfP) shows you all of the opportunities that have closed, or are soon to close, with your SALs. Tracking Order Type is important since TAM team quota and compensation depend on this. Please reference the latest [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/) information to know what is counted.

Next steps for you:

1. Customize [this SFDC report](https://gitlab.my.salesforce.com/00O4M000004agfP) where “Account Owner = your SALs”; “TAM = You”
1. Save report
1. Subscribe to report when “Record Count Greater Than 0” and Frequency = Weekly (You’ll get a weekly email as a reminder to look at the report)
1. If you find an opp that is tagged incorrectly, chatter (@Sales-Support) in the opportunity and let them know there is a mistake ([example(https://about.gitlab.com/handbook/customer-success/tam/#tam-tools)])

## Related pages

- [Dogfooding in Customer Success](/handbook/customer-success/#dogfooding)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions)
- [Commercial Sales Customer Success](/handbook/customer-success/comm-sales/)
- [Customer Success' FAQ](/handbook/customer-success/faq/)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [GitLab Positioning](/handbook/positioning-faq/)
- [Product Stages and the POCs for each](/handbook/product/categories/#devops-stages)
- [How to Provide Feedback to Product](/handbook/product/how-to-engage/#feedback-template)
- [Sales handbook](/handbook/sales/)
- [Support handbook](/handbook/support/)
- [Workshops and Lunch-and-Learn slides](https://drive.google.com/drive/folders/1qAymFTiXFEk-lRSNreIhaZ6Z62fdo_y2)
